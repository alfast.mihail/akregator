class CreateArticles < ActiveRecord::Migration[6.0]
  def change
    create_table :articles do |t|
      t.text :title
      t.text :link
      t.text :text

      t.timestamps
    end
  end
end
