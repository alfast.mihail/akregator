class AddLinkIndexToArticles < ActiveRecord::Migration[6.0]
  def change
	  add_index :articles, :link, length: 500, unique: true
  end
end
