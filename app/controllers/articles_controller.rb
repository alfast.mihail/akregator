class ArticlesController < ApplicationController
	def index
		@articles = Article.all
	end
	
	def latest_news
		@articles = Article.all
	end
	
	def scrape
		agent = Mechanize.new
		page = agent.get('https://ria.ru/lenta/')
		
		newz = page.xpath("//a[@class='list-item__title color-font-hover-only']")
		
		newz.each do |str|
			link = str["href"]
			if !Article.find_by(link: link)
				title = str.inner_text
				content = agent.get(link).xpath("//div[@class='article__body js-mediator-article mia-analytics']/div[@data-type='text' or @class='article__quote-text m-small']")
				article = Article.new
				article.title = title
				article.link = link
				article.text = content.inner_text
				article.save!
			end
		end
		redirect_to(action: 'latest_news')
	end
end
